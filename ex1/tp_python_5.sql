-- --------------------------------------------------------

--
-- Table structure for table `Commune`
--

CREATE TABLE `Commune` (
  `code_arrondissement` int(11) NOT NULL,
  `code_canton` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `population_total` int(11) NOT NULL,
  `code_departement` int(11) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Departement`
--

CREATE TABLE `Departement` (
  `code_departement` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `code_region` int(11) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `Region`
--

CREATE TABLE `Region` (
  `code_region` int(11) NOT NULL,
  `nom` int(11) NOT NULL
);
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
