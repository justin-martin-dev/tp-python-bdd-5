import csv, sqlite3

con = sqlite3.connect('../database.db')
cursor = con.cursor()

def init_database():
    sql_file = open('./tp_python_5.sql')
    sql_as_string = sql_file.read()
    cursor.executescript(sql_as_string)

def feed_region():
    with open('./data/regions.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        region_to_db = [(row[0], row[1]) for row in spamreader]
        cursor.executemany("INSERT INTO Region VALUES (?, ?);",region_to_db)

def feed_departement():
    with open('./data/departements.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        departement_to_db = [(row[2], row[3], row[0]) for row in spamreader]
        cursor.executemany("INSERT INTO Departement VALUES (?, ?, ?);", departement_to_db)

def feed_commune():
    with open('./data/communes.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        commune_to_db = [(row[3], row[4], row[6], row[9].replace(" ", ""), row[2]) for row in spamreader]
        cursor.executemany("INSERT INTO Commune VALUES (?, ?, ?, ?, ?);", commune_to_db)

init_database()
feed_region()
feed_departement()
feed_commune()

con.commit()