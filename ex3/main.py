import sqlite3
from tabulate import tabulate

con = sqlite3.connect('../database.db')
cursor = con.cursor()

print("------------ Commune with the same name --------------")
cursor.execute("SELECT Commune.nom, GROUP_CONCAT(Commune.code_departement)"
               " AS groupe_concat FROM Commune GROUP BY Commune.nom HAVING groupe_concat LIKE '%,%'")
print(tabulate(cursor, headers=["Nom Commune", "Numéros de département"]))
cursor.close()
