import sqlite3
from xml.etree.ElementTree import Element, SubElement, tostring, parse

con = sqlite3.connect('../database.db')
cursor = con.cursor()

root = Element('root')

#------------  SAVING  -------------------
def save_communes():
    cursor.execute("SELECT * FROM Commune")

    communesElement = SubElement(root, 'Communes')
    communes = cursor.fetchall()

    for commune in communes:
        communeElement = SubElement(communesElement, 'Commune')
        communeElement.set("code_arrondissement", str(commune[0]))
        communeElement.set("code_canton", str(commune[1]))
        communeElement.set("nom", commune[2])
        communeElement.set("population_total", str(commune[3]))
        communeElement.set("code_departement", str(commune[4]))

def save_departements():
    cursor.execute("SELECT * FROM Departement")

    departementsElement = SubElement(root, 'Departements')
    departements = cursor.fetchall()

    for departement in departements:
        departementElement = SubElement(departementsElement, 'Departement')
        departementElement.set("code_departement", str(departement[0]))
        departementElement.set("nom", departement[1])
        departementElement.set("code_region", str(departement[2]))

def save_regions():
    cursor.execute("SELECT * FROM Region")

    regionsElement = SubElement(root, 'Regions')
    regions = cursor.fetchall()

    for region in regions:
        regionElement = SubElement(regionsElement, 'Region')
        regionElement.set("code_region", str(region[0]))
        regionElement.set("nom", region[1])

def save_database(pathname):
    save_communes()
    save_departements()
    save_regions()

    data = tostring(root)
    databaseBackup = open(pathname, 'wb')
    databaseBackup.write(data)
    databaseBackup.close()
#----------------------------------------

def clear_database():
    cursor.execute("Delete FROM Commune WHERE 1")
    cursor.execute("Delete FROM Region WHERE 1")
    cursor.execute("Delete FROM Departement WHERE 1")

    con.commit()

def upload_communes(communesElement):
    print('Starting upload communes.....')
    communes_to_db = [(communeElement.attrib["code_arrondissement"], communeElement.attrib["code_canton"],
                       communeElement.attrib["nom"], int(communeElement.attrib["population_total"]),
                       communeElement.attrib["code_departement"]) for communeElement in communesElement]
    cursor.executemany("INSERT INTO Commune VALUES (?, ?, ?, ?, ?);", communes_to_db)

def upload_departements(departementsElement):
    print('Starting upload departements.....')
    communes_to_db = [(departementElement.attrib["code_departement"], departementElement.attrib["nom"], departementElement.attrib["code_region"])
                      for departementElement in departementsElement]
    cursor.executemany("INSERT INTO Departement VALUES (?, ?, ?);", communes_to_db)

def upload_regions(regionsElement):
    print('Starting upload departements.....')
    communes_to_db = [(regionElement.attrib["code_region"], regionElement.attrib["nom"]) for regionElement in regionsElement]
    cursor.executemany("INSERT INTO Region VALUES (?, ?);", communes_to_db)

def upload_backup(pathname, with_clear=False):
    if with_clear:
        clear_database()

    tree = parse(pathname)
    root = tree.getroot()

    upload_communes(root.find("Communes"))
    upload_departements(root.find("Departements"))
    upload_regions(root.find("Regions"))

    con.commit()

#save_database("backup.xml")
upload_backup("backup.xml", True)

cursor.close()
