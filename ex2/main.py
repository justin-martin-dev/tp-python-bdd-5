import sqlite3
from tabulate import tabulate

con = sqlite3.connect('../database.db')
cursor = con.cursor()

def calc_pop_departement():
    print("-------- Population by Departement ---------")
    cursor.execute("SELECT Departement.nom, sum(Commune.population_total) FROM Commune JOIN Departement on Commune.code_departement = Departement.code_departement GROUP BY Departement.nom")
    print(tabulate(cursor, headers=["Nom département", "Population total"]))

def calc_pop_region():
    print("------------ Population by Region --------------")
    cursor.execute("SELECT Region.nom, sum(Commune.population_total) FROM Commune JOIN Departement on Commune.code_departement = Departement.code_departement JOIN Region on Departement.code_region = Region.code_region GROUP BY Region.nom")
    print(tabulate(cursor, headers=["Nom Région", "Population total"]))

calc_pop_departement()
print("")
calc_pop_region()
cursor.close()
